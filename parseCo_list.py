import csv

with open('Test File.csv') as csv_file:
  csv_reader = csv.reader(csv_file, delimiter=',')
  line_count = 0
  for row in csv_reader:
    if line_count == 0:
      print('Header:' + row[0] + ','+ row[1] + ','+ row[2] + ','+ row[3])
      line_count += 1
    else:
      print(str(line_count) + ',' + row[0] + ',' + row[1] + ',' + row[2] + ',' + row[3])
      line_count += 1
  print('Processed ' + str(line_count) + ' lines')


#rep count
import csv

with open('Test File.csv') as csv_file:
  csv_reader = csv.reader(csv_file, delimiter=',')
  line_count = 0
  RepCount = 0
  for row in csv_reader:
    if line_count == 0:
      line_count += 1
      continue;
    elif line_count > 0:
      RepCount = 0
      for cell in range(2,3):
        if cell is empty:
          continue;
        else:
          RepCount += 1;
      print('Rep Count ' + str(RepCount) + ' Reps')

  print('Processed ' + str(line_count) + ' lines')



#working on this
import csv
import array as cellsOfCurrentRow
import array as row
import numpy as np

line_count = 0
maxNumOfCols = 4
rowArray = []
actualCSV = []
company = []
splitList = []
rep = []
with open('test_file.csv') as csv_file:
  csv_reader = csv.reader(csv_file, delimiter=',')
  for row in csv_reader:
    if line_count == 0:
      line_count += 1
      continue;
    elif line_count > 0:
      cellsOfCurrentRow = []
      for cell in row:
        cellsOfCurrentRow.append(cell)
      rowArray.append(cellsOfCurrentRow)
  for i in rowArray:
    companyInfoCells = 0
    currentCompany = []
    currentRep = []
    for j in i:
      if(companyInfoCells <= 1):
        currentCompany.append(j)
        companyInfoCells += 1;
      elif (companyInfoCells >= 2 and companyInfoCells <= 3 ):
        currentRep.append(j)
        companyInfoCells += 1;
    company = [currentCompany, currentRep]
    splitList.append(company)

print(splitList)
