# Importing important packages for program execution:
import time
import sys
import csv

# Initializing Variables:

# This variable stores the file name
csvFileName = ""

# newFileName is the output file name/formmated list
newFileName = ""

# Maximum number of columns in the file
maxColumns = 0

# Maximum possible of company contacts that each company could have
maxContacts = 0

# Maximum number of columns of company contact info  per company contact
# This number will be usuall 5 because: first, last, title, email, phone number
maxContactInfo = 0

# maxNumOfCoContactInfo = The maximum possible number of company contact info columns (taking into account all company contacts) for any given company.
maxNumOfCoContactInfo = 0

# numCompanyColumns stores the amount of columns that will be copied for each company.
numCompanyColumns = 0

# ------------------- #

#Defining Routines:

# Little animation to simulate loading bar
def loadingBar():
	print("")
	animation = "|/-\\"

	for i in range(7):
		time.sleep(0.1)
		sys.stdout.write("\r" + animation[i % len(animation)])
		sys.stdout.flush()
	print("* * * * * * * * * *")
	print("")

# Collecting some input from the user
def inputCollector():
	print("Collecting your input:\n")
	time.sleep(2)
	global csvFileName

	csvFileName = input("What is the name of the file that you would like to parse?\n")
	time.sleep(.5)

	while (True):
		global maxContacts
		global maxContactInfo
		global newFileName

		maxContacts = int(input("What is the maximum number of company contacts that a company could have?\n"))
		time.sleep(.5)
		maxContactInfo = int(input("How many columns per company contacts (First, last, email, phone, etc.)?\n"))
		time.sleep(.5)

		loadingBar()

		print("Please verify input:")
		time.sleep(1)
		print("What is the maximum number of company contacts that a company could have? \n Your answer:" + str(maxContacts))
		time.sleep(1)
		print("How many columns per company contacts (First, last, email, phone, etc.)? \n Your answer:" + str(maxContactInfo))
		time.sleep(2)
		print("")

		verified = input("Is this accurate?\nPlease answer \'y\' or \'n\':\n")
		if verified == "y" :
			time.sleep(1)
			print("\nInput was verified.")
			break
		else:
			print("\nOk, let's try again.")
			loadingBar()

	time.sleep(.5)
	newFileName = input("\nWhat would you like to call the output file which contains the new, formatted list?\n")
	time.sleep(.5)


# When calling the following routine, the variables maxNumOfCoContactInfo & numCompanyColumns will get values
def calculateVars():
	#Calculating the rest of needed vars
	print("Calculating other variables...\n")
	time.sleep(2)
	global maxNumOfCoContactInfo
	global numCompanyColumns
	global maxColumns

	# This looks into the file to count the number of columns that the program will be working with
	with open(csvFileName) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=',')
		maxColumns = len(next(csv_reader))

	print("The amount of columns in this file is " + str(maxColumns))
	time.sleep(1)

	maxNumOfCoContactInfo = maxContacts * maxContactInfo
	print("The maximum possible number of company contact info columns (taking into account all company contacts) for any given company is: " + str(maxNumOfCoContactInfo))
	time.sleep(1)

	global numCompanyColumns
	numCompanyColumns = maxColumns - maxNumOfCoContactInfo
	print("The amount of columns that will be copied over for each company contact when a new line is created is: " + str(numCompanyColumns))
	time.sleep(1)

# This function reads the raw csv File, then creates a new file with curated company list
def parser():
	with open(csvFileName) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=',')

		with open(newFileName,'w') as new_file:
			csv_writer = csv.writer(new_file)

			for row in csv_reader:
				# Get all the cells that will be copied multiple time for any given company

				# Array for numCompanyColumns
				numCompanyColumnsArr = []

				# Array for company contacts
				companyContactsArr = []

				# We use this variable to compare with numCompanyColumns, 
				#and make sure that we copy cells (from a single row) up to the start of the company contact info
				cellCount = 0 
				
				# This first loop grabs the information of a company up to the contact info columns
				for cell in row:
					# Here we build the company info array that we will be copying over.
					if cellCount < numCompanyColumns:
						numCompanyColumnsArr.append(cell)
						cellCount += 1
					elif cellCount >= numCompanyColumns:
						companyContactsArr.append(cell)
					else:
						# it will only break out of the loop when it reaches the contact info columns
						break

				# This second loop has two purposes:
				# 1. To copy over the first part of the row (Company Information) to the same amount of rows as there are company contacts
				# 2. To single out the set of company info cells of a single contact and add it to the row of company information
				contactInfoCount = 0
				copyArr = numCompanyColumnsArr.copy()

				for contact in companyContactsArr:
					# contactInfoCount: this variable keeps track of the cells of a single contact
					# it willbe reset during each iteration so it counts for the new contact

					if contactInfoCount < maxContactInfo:
						copyArr.append(contact)
						contactInfoCount += 1

						if contactInfoCount == maxContactInfo:
							csv_writer.writerow(copyArr)
					else:
						copyArr.clear()
						copyArr = numCompanyColumnsArr.copy()
						copyArr.append(contact)

						contactInfoCount = 1

# Main Routine
def run():
	time.sleep(1)
	print("")
	print("Initializing Program...")
	loadingBar()
	time.sleep(2)

	inputCollector()
	loadingBar()
	time.sleep(2)

	time.sleep(2)
	calculateVars()

	parser()

	print("\n DONE! \n")

	print("   The Name of your new file is: " +  newFileName)

#Program Execution
run()