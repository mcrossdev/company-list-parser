Read Me

Purpose
To facilitate the repetitive, and time consuming process of formatting a company list that has company contacts in columns instead of ROWS where they are more useful for many applications.


******************************

Instructions:

File formatting
The main this is to have all company contact info columns all the way the right. Please review the sample_file.csv file for more guidince.

Execution
1. Get the program (parser.py) and the file you want to parse in the directory/folder.
2. Open Terminal, Navigate to the directory where the parser is, and then type this command "python parser.py" [do not include quotation marks.]

User input
What is the name of the file that you would like to parse?
 [-] enter the exact name of the file

What is the maximum number of company contacts that a company could have?
 [-] Maximum possible of company contacts that each company could have

How many columns per company contacts (First, last, email, phone, etc.)?
 [-] Maximum number of columns of company contact info  per company contact. This number will be usually 5 because: first, last, title, email, phone number

What would you like to call the output file which contains the new, formatted list?
 [-] This is the name of the output file with the new list

There is going to be one more input: 'y' or 'n'. This is is to make sure that you entered the right answers to the questions about company contacts

******************************

Possible questions you may have.
[-] What happens if some cells contain commas? Will the program split the cell into two, or still read the cell as one?

 - The Python CSV reader is really smart because it is able to differentiate columns (which are separeted by commas) and cells that contain commas within them. So you do not need to worry about this edge case.

